#include "header.hh"
/*
GLUI_Checkbox   *checkbox;
GLUI_Spinner    *spinner;
GLUI_RadioGroup *radio;
GLUI_EditText   *edittext;
GLUI_Listbox *listbox;  

void control_cb( int control )
{
  printf( "callback: %d\n", control );

}


void idle( void )
{
  
  if ( glutGetWindow() != mainWindow ) 
    glutSetWindow(mainWindow);  

  glutPostRedisplay();
}

void call_GLUI()
{
  GLUI_Master.set_glutIdleFunc( do_one_simulation_step ); 
   

  //glutPostRedisplay();
  GLUI *glui = GLUI_Master.create_glui("GLUI", 0 );
  
    
      GLUI_Panel *glui_panel = glui->add_panel ("Object Properties");

						   
      new GLUI_StaticText( glui_panel, "Draw Smoke" );
      new GLUI_Separator( glui_panel );
      
      //DrawSmoke
      checkbox = new GLUI_Checkbox( glui_panel, "Draw Smoke", &draw_smoke, 1, control_cb );
      
      checkbox = new GLUI_Checkbox( glui_panel, "Draw Vectors", &draw_vecs, 1, control_cb );
      
      //ColorMap
      listbox = new GLUI_Listbox(glui, "Colormap", &scalar_col);
      listbox->add_item(0,"Black and White");
      listbox->add_item(1,"Rainbow");
      listbox->add_item(2,"Rainbow Banded");
      listbox->add_item(3,"Heatmap");
      listbox->add_item(4,"Green Fire"); 
      listbox->add_item(5,"Zebra");       
      
      new GLUI_StaticText( glui, "Glyphs" );
      
      listbox = new GLUI_Listbox(glui, "Scalar fields", &glyph_select);
      listbox->add_item(0,"Black and White");
      listbox->add_item(1,"Density");
      listbox->add_item(2,"Velocity");
      listbox->add_item(3,"Force");

      listbox = new GLUI_Listbox(glui, "Vector Fields", &vector_field);
      listbox->add_item(0,"Fluid Velocity");
      listbox->add_item(1,"Force Field");

      
      spinner = new GLUI_Spinner( glui, "Number of colors:", &nr_col);
      spinner->set_int_limits( 2, 256 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT );
      
      spinner = new GLUI_Spinner( glui, "Number of vectors:", &divider);
      spinner->set_int_limits( 1, 10 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT );
      
      listbox = new GLUI_Listbox(glui, "Vector_type", &vector_type);
      listbox->add_item(0,"Arrows");
      listbox->add_item(1,"Torus");

      
  glui->set_main_gfx_window( mainWindow );
 // GLUI_Master.set_glutIdleFunc( idle ); 

}*/


