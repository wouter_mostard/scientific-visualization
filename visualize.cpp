#include "header.hh"

extern fftw_real *vx, *vy;             //(vx,vy)   = velocity field at the current moment
extern fftw_real *vx0, *vy0;           //(vx0,vy0) = velocity field at the previous moment
extern fftw_real *fx, *fy;	            //(fx,fy)   = user-controlled simulation forces, steered with the mouse
extern fftw_real *rho, *rho0;			//smoke density at the current (rho) and previous (rho0) moment
extern float divergence_v[DIM*DIM], divergence_f[DIM*DIM];

float min_rho;
float max_rho;
float min_fvm;
float max_fvm ;
float min_ffm ;
float max_ffm ;
float min_div_v ;
float max_div_v ;
float min_div_f ;
float max_div_f ;

float magnit(float x, float y)
{
 return sqrt(x* x + y * y);  
}

    

void visualize(void)
{
	int        i, j, idx; double px,py; float max_rho = 1.0, ff_mag, fv_mag;
	fftw_real  wn = (fftw_real)winWidth / (fftw_real)(DIM + 1);   // Grid cell width
	fftw_real  hn = (fftw_real)winHeight / (fftw_real)(DIM + 1);  // Grid cell heigh
	
	if (draw_smoke)
	{	
		int idx0, idx1, idx2, idx3;
		double px0, py0, px1, py1, px2, py2, px3, py3;
		float cur_fvm, cur_ffm, cur_rho;
		min_rho = 1;
		max_rho = 0;
		min_fvm = 1;
		max_fvm = 0;
		min_ffm = 1;
		max_ffm = 0;
		min_div_v = 1000;
		max_div_v = -100;
		min_div_f = 1000;
		max_div_f = -100;
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glBegin(GL_TRIANGLES);
		
		
		
		for (j = 0; j < DIM - 1; j++)            //draw smoke
		{
			for (i = 0; i < DIM - 1; i++)
			{
			  idx = (j * DIM) + i;
			  
			  cur_fvm = magnit(vx[idx], vy[idx]);
			  cur_ffm = magnit(fx[idx], fy[idx]);
			  cur_rho = rho[idx];
			  if (cur_rho < min_rho)
			  {  min_rho = cur_rho;}
			  if (cur_rho > max_rho)
			  {  max_rho = cur_rho;}
			  if (cur_fvm < min_fvm)
			  {  min_fvm = cur_fvm;}
			  if (cur_fvm  > max_fvm)
			  {  max_fvm = cur_fvm;}
			  if (cur_ffm < min_ffm)
			  {  min_ffm = cur_ffm;}
			  if (cur_ffm > max_ffm)
			  {  max_ffm = cur_ffm;}
			  
			  divergence_v[idx] = 0.5* (vx[idx+1] - vx[idx-1]) + 0.5*( vy[idx+DIM] - vy[idx-DIM]);
			  
			  if (divergence_v[idx] < min_div_v)
			  {
			   min_div_v = divergence_v[idx]; 
			  }
			  if (divergence_v[idx] > max_div_v)
			    max_div_v = divergence_v[idx];
			  
			
			
			  divergence_f[idx] = 0.5* (fx[idx+1] - fx[idx-1]) + 0.5*( fy[idx+DIM] - fy[idx-DIM]);
			  
			  if (divergence_f[idx] < min_div_f)
			  {
			   min_div_f = divergence_f[idx]; 
			  }
			  if (divergence_f[idx] > max_div_f)
			    max_div_f = divergence_f[idx];
			  
			}
		}
		

			    
			  
		
		
		
		
		
		for (j = 0; j < DIM - 1; j++)            //draw smoke
		{
			for (i = 0; i < DIM - 1; i++)
			{
				px0 = wn + (fftw_real)i * wn;
				py0 = hn + (fftw_real)j * hn;
				idx0 = (j * DIM) + i;


				px1 = wn + (fftw_real)i * wn;
				py1 = hn + (fftw_real)(j + 1) * hn;
				idx1 = ((j + 1) * DIM) + i;


				px2 = wn + (fftw_real)(i + 1) * wn;
				py2 = hn + (fftw_real)(j + 1) * hn;
				idx2 = ((j + 1) * DIM) + (i + 1);


				px3 = wn + (fftw_real)(i + 1) * wn;
				py3 = hn + (fftw_real)j * hn;
				idx3 = (j * DIM) + (i + 1);

				if (draw_3d==0) 
				{
				  if (scalar_type == 0)
				  {
				    set_colormap(rho[idx0], min_rho, max_rho);    glVertex2f(px0, py0);
				    set_colormap(rho[idx1], min_rho, max_rho);    glVertex2f(px1, py1);
				    set_colormap(rho[idx2], min_rho, max_rho);    glVertex2f(px2, py2);
				  
				    set_colormap(rho[idx0], min_rho, max_rho);    glVertex2f(px0, py0);
				    set_colormap(rho[idx2], min_rho, max_rho);    glVertex2f(px2, py2);
				    set_colormap(rho[idx3], min_rho, max_rho);    glVertex2f(px3, py3);
				  }
				  if (scalar_type == 1)
				  {
				    set_colormap(magnit(vx[idx0], vy[idx0]),  min_fvm, max_fvm);    glVertex2f(px0, py0);
				    set_colormap(magnit(vx[idx1], vy[idx1]), min_fvm, max_fvm);    glVertex2f(px1, py1);
				    set_colormap(magnit(vx[idx2], vy[idx2]),  min_fvm, max_fvm);    glVertex2f(px2, py2);
				  
				    set_colormap(magnit(vx[idx0], vy[idx0]), min_fvm, max_fvm);    glVertex2f(px0, py0);
				    set_colormap(magnit(vx[idx2], vy[idx2]), min_fvm, max_fvm);    glVertex2f(px2, py2);
				    set_colormap(magnit(vx[idx3], vy[idx3]), min_fvm, max_fvm);    glVertex2f(px3, py3);  
				  }
				  if (scalar_type == 2)
				  {
				    set_colormap(magnit(fx[idx0], fy[idx0]),  min_ffm, max_ffm);    glVertex2f(px0, py0);
				    set_colormap(magnit(fx[idx1], fy[idx1]),  min_ffm, max_ffm);    glVertex2f(px1, py1);
				    set_colormap(magnit(fx[idx2], fy[idx2]),  min_ffm, max_ffm);    glVertex2f(px2, py2);
				  
				    set_colormap(magnit(fx[idx0], fy[idx0]), min_ffm, max_ffm);    glVertex2f(px0, py0);
				    set_colormap(magnit(fx[idx2], fy[idx2]), min_ffm, max_ffm);    glVertex2f(px2, py2);
				    set_colormap(magnit(fx[idx3], fy[idx3]), min_ffm, max_ffm);    glVertex2f(px3, py3);  
				  }
				  if (scalar_type == 3)
				  {	
				    set_colormap(divergence_v[idx0],  min_div_v, max_div_v);    glVertex2f(px0, py0);
				    set_colormap(divergence_v[idx1], min_div_v, max_div_v);    glVertex2f(px1, py1);
				    set_colormap(divergence_v[idx2],  min_div_v, max_div_v);    glVertex2f(px2, py2);
				  
				    set_colormap(divergence_v[idx0], min_div_v, max_div_v);    glVertex2f(px0, py0);
				    set_colormap(divergence_v[idx2], min_div_v, max_div_v);    glVertex2f(px2, py2);
				    set_colormap(divergence_v[idx3], min_div_v, max_div_v);    glVertex2f(px3, py3);  
				  }
				  if (scalar_type == 4)
				  {	
				    set_colormap(divergence_f[idx0],  min_div_f, max_div_f);    glVertex2f(px0, py0);
				    set_colormap(divergence_f[idx1], min_div_f, max_div_f);    glVertex2f(px1, py1);
				    set_colormap(divergence_f[idx2],  min_div_f, max_div_f);    glVertex2f(px2, py2);
				  
				    set_colormap(divergence_f[idx0], min_div_f, max_div_f);    glVertex2f(px0, py0);
				    set_colormap(divergence_f[idx2], min_div_f, max_div_f);    glVertex2f(px2, py2);
				    set_colormap(divergence_f[idx3], min_div_f, max_div_f);    glVertex2f(px3, py3);  
				  }
				}
				if (draw_3d == 1)
				{
				    int v = 2;
				    set_colormap(rho[idx0], min_rho, max_rho);    glVertex3f(px0, py0, v * rho[idx0]);
				    set_colormap(rho[idx1], min_rho, max_rho);    glVertex3f(px1, py1, v * rho[idx1]);
				    set_colormap(rho[idx2], min_rho, max_rho);    glVertex3f(px2, py2, v * rho[idx2]);
				  
				    set_colormap(rho[idx0], min_rho, max_rho);    glVertex3f(px0, py0, v * rho[idx0]);
				    set_colormap(rho[idx2], min_rho, max_rho);    glVertex3f(px2, py2, v * rho[idx2]);
				    set_colormap(rho[idx3], min_rho, max_rho);    glVertex3f(px3, py3, v * rho[idx3]);
				}
				  
			}
		}
		glEnd();
	}
	/*if (draw_smoke)
	{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	for (j = 0; j < DIM - 1; j++)			//draw smoke
	{
		glBegin(GL_TRIANGLE_STRIP);

		i = 0;
		px = wn + (fftw_real)i * wn;
		py = hn + (fftw_real)j * hn;
		idx = (j * DIM) + i;
		glColor3f(rho[idx]/max_rho,rho[idx]/max_rho,rho[idx]/max_rho);
		glVertex2f(px,py);

		for (i = 0; i < DIM - 1; i++)
		{
			px = wn + (fftw_real)i * wn;
			py = hn + (fftw_real)(j + 1) * hn;
			idx = ((j + 1) * DIM) + i;
			set_colormap(rho[idx]/max_rho);
			glVertex2f(px, py);
			px = wn + (fftw_real)(i + 1) * wn;
			py = hn + (fftw_real)j * hn;
			idx = (j * DIM) + (i + 1);
			set_colormap(rho[idx]/max_rho);
			glVertex2f(px, py);
		}

		px = wn + (fftw_real)(DIM - 1) * wn;
		py = hn + (fftw_real)(j + 1) * hn;
		idx = ((j + 1) * DIM) + (DIM - 1);
		set_colormap(rho[idx]/max_rho);
		glVertex2f(px, py);
		glEnd();
	}
	}*/
	if (draw_vecs)
	{	
		if(vector_type == 0)
		{
		  //int Vec_DIM = DIM /divider;
		  glBegin(GL_LINES);				//draw velocities
		  for (i = 0; i < DIM; i+=divider)
			  for (j = 0; j < DIM; j+=divider)
			  {
				  idx = (j * DIM) + i;
				  select_color(idx);
				  
				  glVertex2f(wn + (fftw_real)i * wn, hn + (fftw_real)j * hn);
				  
				  if(vector_field){
				    glVertex2f((wn + (fftw_real)i * wn) - 0.5 * vec_scale * fx[idx], (hn + (fftw_real)j * hn) - 0.5 * vec_scale * fy[idx]);
				  }
				  else
				  {
				    glVertex2f((wn + (fftw_real)i * wn) - 0.5 * vec_scale * vx[idx], (hn + (fftw_real)j * hn) - 0.5 * vec_scale * vy[idx]);					  
				  }
			  
			  }
			  
		  glEnd();
		  
		  glBegin(GL_TRIANGLES);				//draw velocities
		  for (i = 0; i < DIM; i+=divider)
			  for (j = 0; j < DIM; j+=divider)
			  {
				  idx = (j * DIM) + i;
				  select_color(idx);
				  if(vector_field){
				    glVertex2f((wn + (fftw_real)i * wn) - 0.25 * vec_scale * fx[idx], (hn + (fftw_real)j * hn) + 0.25 * vec_scale * fy[idx]);
				    glVertex2f((wn + (fftw_real)i * wn) + 0.25 * vec_scale * fx[idx], (hn + (fftw_real)j * hn) -0.25 * vec_scale * fy[idx]);
				    glVertex2f((wn + (fftw_real)i * wn) + 0.5*vec_scale * fx[idx], (hn + (fftw_real)j * hn) + 0.5 *vec_scale * fy[idx]);  
				  }
				  else
				  {
				    glVertex2f((wn + (fftw_real)i * wn) - 0.25 * vec_scale * vy[idx], (hn + (fftw_real)j * hn) + 0.25 * vec_scale * vx[idx]);
				    glVertex2f((wn + (fftw_real)i * wn) + 0.25 * vec_scale * vy[idx], (hn + (fftw_real)j * hn) - 0.25 * vec_scale * vx[idx]);
				    glVertex2f((wn + (fftw_real)i * wn) + 0.5*vec_scale * vx[idx], (hn + (fftw_real)j * hn) + 0.5*vec_scale * vy[idx]);
				  }
			  } 
		  glEnd(); 
		}
	
		if (vector_type == 1)
		{
		/*for (i = 0; i < DIM; i+=divider)
			  for (j = 0; j < DIM; j+=divider)
			  {
				  idx = (j * DIM) + i;
				  select_color(idx);
				  //glColor3d(1,0,0); 
				  glPushMatrix();
				  
				  glTranslatef( wn + (fftw_real)i * wn, hn + (fftw_real)j * hn, 0);
				 // glRotatef( 45, 1.0, 1.0, 1.0);
				  glutWireSphere(10,  50, 50);
				  
				  glPopMatrix();
			  }*/
		glBegin(GL_POLYGON);
		for (i = 0; i < DIM; i+=divider)
		    for (j = 0; j < DIM; j+=divider)
		    {
			    idx = (j * DIM) + i;
			    select_color(idx);
			    
				glVertex2f((wn + (fftw_real)i * wn) - 0.5 * vec_scale * vx[idx], (hn + (fftw_real)j * hn) - 0.5 * vec_scale * vy[idx]);
				glVertex2f((wn + (fftw_real)i * wn) + 0.5 * vec_scale * vx[idx], (hn + (fftw_real)j * hn) - 0.5 * vec_scale * vy[idx]);
				glVertex2f((wn + (fftw_real)i * wn) + 0.5 * vec_scale * vx[idx], (hn + (fftw_real)j * hn) + 0.5 * vec_scale * vy[idx]);
				glVertex2f((wn + (fftw_real)i * wn) - 0.5 * vec_scale * vx[idx], (hn + (fftw_real)j * hn) + 0.5 * vec_scale * vy[idx]);
				glVertex2f((wn + (fftw_real)i * wn) - 0.5 * vec_scale * vx[idx], (hn + (fftw_real)j * hn) - 0.5 * vec_scale * vy[idx]);
			    //glColor3f((wn + (fftw_real)i * wn) - 0.5 * vec_scale * vx[idx], (hn + (fftw_real)j * hn) - 0.5 * vec_scale * vy[idx], 1.0);
				//glVertex3f((wn + (fftw_real)i * wn) - 0.5 * vec_scale * vx[idx], (hn + (fftw_real)j * hn) - 0.5 * vec_scale * vy[idx], 0.0);
				
				
				

		    }
		    glEnd();
		}
	}


	if (vis_isolines == 1)
	  draw_isolines();
	
}