#include <rfftw.h>              //the numerical simulation FFTW library
#include <stdio.h>              //for printing the help text
#include <math.h>               //for various math functions
#include <GL/glut.h>            //the GLUT graphics library
//#include <GL/glui.h>

extern int glyph_select;	
extern int divider;
//Global variables
extern int scalar_col;
extern int mainWindow;

extern float vec_scale;
extern int vector_type;

extern double dt;
extern float visc;
extern int vector_field;

extern int min_iso, max_iso, nr_isolines;

extern int rho_0;

extern int scalar_type;

extern int frozen;
const int DIM = 50;

extern int vis_isolines;
extern int draw_3d;
extern int   winWidth, winHeight;
extern int   draw_smoke ;           //draw the smoke or not
extern int   draw_vecs ; 
//Color Functions -- "colors.cpp"
const int COLOR_BLACKWHITE=0;   //different types of color mapping: black-and-white, rainbow, banded
const int COLOR_RAINBOW=1;
const int COLOR_BANDS=2;
const int COLOR_HEAT=3;
const int COLOR_GREEN=4;
const int COLOR_ZEBRA=5;

extern float min_rho;
extern float max_rho;
extern float min_fvm;
extern float max_fvm ;
extern float min_ffm ;
extern float max_ffm ;

extern float divergence_v[DIM*DIM];
extern float divergence_f[DIM*DIM];

extern int iso_col;

// clamp colors
extern int clamp_col;
extern int minColor;
extern int maxColor;
extern int scale_col;

float calc_intersect(float pi, float vi, float vj, float v_init);
int compute_isolines();
void  draw_isolines();


void legend_nrs(float min, float max);
float max(float x, float y);
void drawLegend(float min_val, float max_val);
void legendBW();
void legendCR();
void legendCH();
void legendCG();
void legendCZ();

void rainbow(float value,float* R,float* G,float* B);
void heatMap(float value, float* R, float* G, float* B);
void greenMap(float value, float* R, float* G, float* B);
void zebraMap(float value, float* R, float* G, float* B);
void set_colormap(float vy, float min_val, float max_val);
void select_color(int idx);
void direction_to_color(float x, float y, int method);



const int GLYPH_DENSITY = 1;
const int GLYPH_FV_MAGN = 2;
const int GLYPH_FF_MAGN = 3;  

void control_cb( int control );
void idle( void );
void call_GLUI();

void visualize(void);


void light();
// simulations.cpp
void init_simulation(int n);
void FFT(int direction,void* vx);
int clamp(float x);
void solve(int n, fftw_real* vx, fftw_real* vy, fftw_real* vx0, fftw_real* vy0, fftw_real visc, fftw_real dt);
void diffuse_matter(int n, fftw_real *vx, fftw_real *vy, fftw_real *rho, fftw_real *rho0, fftw_real dt);
void set_forces(void);
void do_one_simulation_step(void);


// isolines.cpp

