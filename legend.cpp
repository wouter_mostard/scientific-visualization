#include "header.hh"

void drawLegend(float min_val, float max_val)
{
    if (scalar_col==COLOR_BLACKWHITE)
    {
      legendBW();
    }

    else if (scalar_col==COLOR_RAINBOW)
    {
      legendCR();
    }
    
    else if (scalar_col==COLOR_BANDS)
    {
      legendCR();
    }
	  
    else if (scalar_col == COLOR_HEAT)
    {
      legendCH();
    }
    else if (scalar_col == COLOR_GREEN)
    {
      legendCG();
    }
    else if (scalar_col == COLOR_ZEBRA)
    {
      legendCZ();
    }    
    
    legend_nrs(min_val, max_val);
}

void legendBW()
{
   float k;

    glPushMatrix();
    glLoadIdentity();
    
    for (k = 0 ; k < 256 ; k++)
    {
    glColor3f(k/256,k/256 ,k/256);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k, 15);glVertex2f(16+k, 15);
	glTexCoord2f(16+k, 15);glVertex2f(16+k, 50);
	glTexCoord2f(16+k, 50);glVertex2f(15+k, 50);
	glTexCoord2f(15+k, 50);glVertex2f(15+k, 15);
    glEnd();
    }
    
    glPopMatrix();
}

void legendCR()
{
    float k, R, G, B, value;
    const float dx=0.8;
    
    glPushMatrix();
    glLoadIdentity();

    for (k = 0 ; k < 256 ; k++)
    {
    
    value = (6-2*dx)*(k/256)+dx;
    R = max(0.0,(3-fabs(value-4)-fabs(value-5))/2);
    G = max(0.0,(4-fabs(value-2)-fabs(value-4))/2);
    B = max(0.0,(3-fabs(value-1)-fabs(value-2))/2); 
      
    glColor3f(R, G, B);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k, 15);glVertex2f(16+k, 15);
	glTexCoord2f(16+k, 15);glVertex2f(16+k, 50);
	glTexCoord2f(16+k, 50);glVertex2f(15+k, 50);
	glTexCoord2f(15+k, 50);glVertex2f(15+k, 15);
    glEnd();
    }

    glPopMatrix();
   
}

void legendCH()
{
    float k;
    int length_third = ceil(256/3);

    glPushMatrix();
    glLoadIdentity();

    for (k = 0 ; k < length_third; k++)
    {
    glColor3f(k/length_third,0.0,0.0);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k, 15);glVertex2f(16+k, 15);
	glTexCoord2f(16+k, 15);glVertex2f(16+k, 50);
	glTexCoord2f(16+k, 50);glVertex2f(15+k, 50);
	glTexCoord2f(15+k, 50);glVertex2f(15+k, 15);
    glEnd();
    
    glColor3f(1.0, k/length_third, 0.0);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k+length_third, 15);glVertex2f(16+k+length_third, 15);
	glTexCoord2f(16+k+length_third, 15);glVertex2f(16+k+length_third, 50);
	glTexCoord2f(16+k+length_third, 50);glVertex2f(15+k+length_third, 50);
	glTexCoord2f(15+k+length_third, 50);glVertex2f(15+k+length_third, 15);
    glEnd();
    
    glColor3f(1.0, 1.0, k/length_third);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k+length_third*2, 15);glVertex2f(16+k+length_third*2, 15);
	glTexCoord2f(16+k+length_third*2, 15);glVertex2f(16+k+length_third*2, 50);
	glTexCoord2f(16+k+length_third*2, 50);glVertex2f(15+k+length_third*2, 50);
	glTexCoord2f(15+k+length_third*2, 50);glVertex2f(15+k+length_third*2, 15);
    glEnd();
    }    
    
    glPopMatrix();

}

void legendCG()
{
    float k;
    int length_third = ceil(256/3);
    glPushMatrix();
    glLoadIdentity();

    for (k = 0 ; k < length_third; k++)
    {
    glColor3f(0.0,k/length_third,0.0);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k, 15);glVertex2f(16+k, 15);
	glTexCoord2f(16+k, 15);glVertex2f(16+k, 50);
	glTexCoord2f(16+k, 50);glVertex2f(15+k, 50);
	glTexCoord2f(15+k, 50);glVertex2f(15+k, 15);
    glEnd();
    
    glColor3f(0.0,1.0, k/length_third);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k+length_third, 15);glVertex2f(16+k+length_third, 15);
	glTexCoord2f(16+k+length_third, 15);glVertex2f(16+k+length_third, 50);
	glTexCoord2f(16+k+length_third, 50);glVertex2f(15+k+length_third, 50);
	glTexCoord2f(15+k+length_third, 50);glVertex2f(15+k+length_third, 15);
    glEnd();
    
    glColor3f(k/length_third, 1.0,1.0);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k+length_third*2, 15);glVertex2f(16+k+length_third*2, 15);
	glTexCoord2f(16+k+length_third*2, 15);glVertex2f(16+k+length_third*2, 50);
	glTexCoord2f(16+k+length_third*2, 50);glVertex2f(15+k+length_third*2, 50);
	glTexCoord2f(15+k+length_third*2, 50);glVertex2f(15+k+length_third*2, 15);
    glEnd();
    }    
    
    glPopMatrix();

}

void legendCZ()
{
    float k;
    int length_third = ceil(256/3);
    glPushMatrix();
    glLoadIdentity();

    for (k = 0 ; k < length_third; k++)
    {
    glColor3f(0.0,k/length_third,0.0);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k, 15);glVertex2f(16+k, 15);
	glTexCoord2f(16+k, 15);glVertex2f(16+k, 50);
	glTexCoord2f(16+k, 50);glVertex2f(15+k, 50);
	glTexCoord2f(15+k, 50);glVertex2f(15+k, 15);
    glEnd();
    
    glColor3f(0.0,1.0, k/length_third);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k+length_third, 15);glVertex2f(16+k+length_third, 15);
	glTexCoord2f(16+k+length_third, 15);glVertex2f(16+k+length_third, 50);
	glTexCoord2f(16+k+length_third, 50);glVertex2f(15+k+length_third, 50);
	glTexCoord2f(15+k+length_third, 50);glVertex2f(15+k+length_third, 15);
    glEnd();
    
    glColor3f(k/length_third, 1.0,1.0);
    glBegin( GL_QUADS );
	glTexCoord2f(15+k+length_third*2, 15);glVertex2f(16+k+length_third*2, 15);
	glTexCoord2f(16+k+length_third*2, 15);glVertex2f(16+k+length_third*2, 50);
	glTexCoord2f(16+k+length_third*2, 50);glVertex2f(15+k+length_third*2, 50);
	glTexCoord2f(15+k+length_third*2, 50);glVertex2f(15+k+length_third*2, 15);
    glEnd();
    }    
    
    glPopMatrix();

}

void legend_nrs(float min, float max)
{
   int first, second, third;
   float middle;
   middle = (min + max)/2;
   
   first = floor(min);
   second = floor(10*(min-first));
   third = floor(10*(10*(min-first)-second));
  
  
   glColor3f(1.0f, 1.0f, 1.0f);
    char numbers[3];
    sprintf(numbers, "%i.%i%i", first, second, third);
    const char * p1 = numbers;
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2f( 15,55 ) ; 
    glColor3f(1.0f, 1.0f, 1.0f);
    do glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, *p1); while( *(++p1) );
    glPopMatrix();
    
   first = floor(middle);
   second = floor(10*(middle-first));
   third = floor(10*(10*(middle-first)-second));
    
    sprintf(numbers, "%i.%i%i", first, second, third);
    const char * p2 = numbers;
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2f( 133,55 ) ; 
    glColor3f(1.0f, 1.0f, 1.0f);
    do glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, *p2); while( *(++p2) );
    glPopMatrix();
    
   first = floor(max);
   second = floor(10*(max-first));
   third = floor(10*(10*(max-first)-second));
    
    sprintf(numbers, "%i.%i%i", first, second, third);
    const char * p3 = numbers;
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2f( 260,55 ) ; 
    glColor3f(1.0f, 1.0f, 1.0f);
    do glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, *p3); while( *(++p3) );
    glPopMatrix();
    
    
}
