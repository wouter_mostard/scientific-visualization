#include "header.hh"
#include <GL/glui.h>
//--- SIMULATION PARAMETERS ------------------------------------------------------------------------
				//size of simulation grid
double dt = 0.4;				//simulation time step
float visc = 0.001;				//fluid viscosity
rfftwnd_plan plan_rc, plan_cr;  //simulation domain discretization
int clamp_col = 0;
fftw_real *vx, *vy;             //(vx,vy)   = velocity field at the current moment
fftw_real *vx0, *vy0;           //(vx0,vy0) = velocity field at the previous moment
fftw_real *fx, *fy;	            //(fx,fy)   = user-controlled simulation forces, steered with the mouse
fftw_real *rho, *rho0;	
float divergence_v[DIM*DIM];
float divergence_f[DIM*DIM];



extern float min_rho;
extern float max_rho;
extern float min_fvm;
extern float max_fvm ;
extern float min_ffm ;
extern float max_ffm ;



int min_iso = 0;
int max_iso = 100;
int nr_isolines = 1;

int draw_3d = 1;

int scalar_type = 0;

int vis_isolines = 0;

int glyph_select = 0;	
int nr_col = 256; 
float maxvx;
float maxvy;

int rho_0 = 50;

int scale_col = 0;
int divider = 1;

int iso_col = 2;

int minColor = 0;
int maxColor = 0;

//--- VISUALIZATION PARAMETERS ---------------------------------------------------------------------
int   winWidth, winHeight;      //size of the graphics window, in pixels
int   color_dir = 0;            //use direction color-coding or not
float vec_scale = 1000;			//scaling of hedgehogs
int   draw_smoke = 1;           //draw the smoke or not
int   draw_vecs = 0;            //draw the vector field or not
int scalar_col = 0;
          //method for scalar coloring
	//Select method for coloring 

int   frozen = 0;               //toggles on/off the animation
int mainWindow;
//variables for windows. 

int CUR_MIN_MAX = 0;

float f_global = 1;

int vector_field = 0;
const int VECTOR_FIELD_VEL = 0;
const int VECTOR_FIELD_FOR = 1;

int vector_type = 0;

GLUI_Checkbox   *checkbox;
GLUI_Spinner    *spinner;
GLUI_RadioGroup *radio;
GLUI_EditText   *edittext;
GLUI_Listbox *listbox;  


 //------ SIMULATION CODE STARTS HERE -----------------------------------------------------------------

//init_simulation: Initialize simulation data structures as a function of the grid size 'n'.
//                 Although the simulation takes place on a 2D grid, we allocate all data structures as 1D arrays,
//                 for compatibility with the FFTW numerical library.

  
 
    
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	  if (draw_3d == 1 )
	  {
	  //glMatrixMode (GL_PROJECTION);	//2. Set the projection matrix
	  //glLoadIdentity();
	  //gluPerspective(60.0, winWidth/winHeight,1,200);
	 
	  glMatrixMode(GL_MODELVIEW);							//1. Set the modelview matrix (including the camera position and view direction)
	  glLoadIdentity();										
	  gluLookAt(25, 1, 25, 25, 0, 25, 0, 1, 0);
	  
	  // glMatrixMode(GL_PROJECTION);
	 
	  glShadeModel (GL_SMOOTH);
	  
	  }
	
	//glMatrixMode (GL_MODELVIEW);
	visualize();

	
	glFlush();
	glutSwapBuffers();	

	light();
}
	  	/*if (scale_col == 1)
	{
	
	  if (scalar_type == 0)
	    drawLegend(min_rho, max_rho);
	  
	  if (scalar_type == 1)
	    drawLegend(min_fvm, max_fvm);
	  
	  if (scalar_type == 2)
	    drawLegend(min_ffm, max_ffm);
	  
	}
	if (scale_col == 0)
	{
	  drawLegend(0.00, 1.00);
	}*/

//reshape: Handle window resizing (reshaping) events
void reshape(int w, int h)
{
 	 
	  glViewport(0.0f, 0.0f, (GLfloat)w, (GLfloat)h);	
	  
	  if(draw_3d == 0) 
	  {	
	    glMatrixMode(GL_PROJECTION);
	    glLoadIdentity();
	    gluOrtho2D(0.0, (GLdouble)w, 0.0, (GLdouble)h);


	   
	  }
	   winWidth = w; winHeight = h;
}

//keyboard: Handle key presses
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	  case 't': dt -= 0.001; break;
	  case 'T': dt += 0.001; break;
	  case 'c': color_dir = 1 - color_dir; break;
	  case 'S': vec_scale *= 1.2; break;
	  case 's': vec_scale *= 0.8; break;
	  case 'V': visc *= 5; break;
	  case 'v': visc *= 0.2; break;
	  case 'x': draw_smoke = 1 - draw_smoke;
		    if (draw_smoke==0) draw_vecs = 1; break;
	  case 'y': draw_vecs = 1 - draw_vecs;
		    if (draw_vecs==0) draw_smoke = 1; break;
	  case 'm': scalar_col++; if (scalar_col>COLOR_HEAT) scalar_col=COLOR_BLACKWHITE; break;
	  case 'a': frozen = 1-frozen; break;
	  case 'k':	printf("select an integer between 2 and 256:\n");
			scanf("%d",&nr_col);break; 
	  case 'j': CUR_MIN_MAX = 1 - CUR_MIN_MAX; break;
	  case 'q': exit(0);
	}
}



// drag: When the user drags with the mouse, add a force that corresponds to the direction of the mouse
//       cursor movement. Also inject some new matter into the field at the mouse location.
void drag(int mx, int my)
{
	int xi,yi,X,Y; double  dx, dy, len;
	static int lmx=0,lmy=0;				//remembers last mouse location

	// Compute the array index that corresponds to the cursor location
	xi = (int)clamp((double)(DIM + 1) * ((double)mx / (double)winWidth));
	yi = (int)clamp((double)(DIM + 1) * ((double)(winHeight - my) / (double)winHeight));

	X = xi; Y = yi;

	if (X > (DIM - 1))  X = DIM - 1; if (Y > (DIM - 1))  Y = DIM - 1;
	if (X < 0) X = 0; if (Y < 0) Y = 0;

	// Add force at the cursor location
	my = winHeight - my;
	dx = mx - lmx; dy = my - lmy;
	len = sqrt(dx * dx + dy * dy);
	if (len != 0.0) {  dx *= 0.1 / len; dy *= 0.1 / len; }
	fx[Y * DIM + X] += dx;
	fy[Y * DIM + X] += dy;
	rho[Y * DIM + X] = 10.0f;
	lmx = mx; lmy = my;
}



void control_cb( int control )
{
  printf( "callback: %d\n", control );

}


void idle( void )
{
  
  if ( glutGetWindow() != mainWindow ) 
    glutSetWindow(mainWindow);  

  glutPostRedisplay();
}

void call_GLUI()
{
  GLUI_Master.set_glutIdleFunc( do_one_simulation_step ); 
  GLUI *glui = GLUI_Master.create_glui("GLUI", 0 );
  
    
      GLUI_Panel *glui_panel = glui->add_panel ("Object Properties");

						   
      new GLUI_StaticText( glui_panel, "The basic visualization can be adjusted here." );
      new GLUI_Separator( glui_panel );
      
      //DrawSmoke
      checkbox = new GLUI_Checkbox( glui_panel, "Draw Smoke", &draw_smoke, 1, control_cb );
      checkbox = new GLUI_Checkbox( glui_panel, "Draw Vectors", &draw_vecs, 1, control_cb );
      
      //ColorMap
      GLUI_Panel *panel_color = glui->add_panel ("Collormapping");
      listbox = new GLUI_Listbox(panel_color, "Scalar Value", &scalar_type);
      listbox->add_item(0,"Fluid Density");
      listbox->add_item(1,"Fluid Velocity Magnitude");
      listbox->add_item(2,"Force Field Magnitude");
      listbox->add_item(3,"Divergence of Fluid Velocity" );
      listbox->add_item(3,"Divergence of Force Field" );

      
      listbox = new GLUI_Listbox(panel_color, "Colormap", &scalar_col);
      listbox->add_item(0,"Black and White");
      listbox->add_item(1,"Rainbow");
      listbox->add_item(2,"Rainbow Banded");
      listbox->add_item(3,"Heatmap");
      listbox->add_item(4,"Green Fire"); 
      listbox->add_item(5,"Zebra");       
      
       checkbox = new GLUI_Checkbox( panel_color, "Scale colors", &scale_col, 1, control_cb );

      
      spinner = new GLUI_Spinner( panel_color, "Number of colors:", &nr_col);
      spinner->set_int_limits( 2, 256 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT );
      
      checkbox = new GLUI_Checkbox( panel_color, "Clamp Colors", &clamp_col, 1, control_cb );
      spinner = new GLUI_Spinner( panel_color, "Min:", &minColor);
      spinner->set_int_limits( 0, 10 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT );
      spinner = new GLUI_Spinner( panel_color, "Max:", &maxColor);      
      spinner->set_int_limits( 0, 10 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT );
      
      GLUI_Panel *panel_glyphs = glui->add_panel ("Glyphs");
      
      listbox = new GLUI_Listbox(panel_glyphs, "Scalar fields", &glyph_select);
      listbox->add_item(0,"Black and White");
      listbox->add_item(1,"Density");
      listbox->add_item(2,"Velocity");
      listbox->add_item(3,"Force");

      listbox = new GLUI_Listbox(panel_glyphs, "Vector Fields", &vector_field);
      listbox->add_item(0,"Fluid Velocity");
      listbox->add_item(1,"Force Field");

          
      spinner = new GLUI_Spinner( panel_glyphs, "Number of vectors:", &divider);
      spinner->set_int_limits( 1, 10 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT );
      
      listbox = new GLUI_Listbox(panel_glyphs, "Vector_type", &vector_type);
      listbox->add_item(0,"Arrows");
      listbox->add_item(1,"Torus");


 
      
      // ********************* DRAW ISOLINES *********************
     
      GLUI_Panel *isolines = glui->add_panel ("Isolines");
      new GLUI_StaticText( isolines, "Change the information of the isolines here." );
      new GLUI_Separator( isolines );
      
      checkbox = new GLUI_Checkbox( isolines, "Draw Isolines", &vis_isolines, 1, control_cb );

      
      spinner = new GLUI_Spinner( isolines, "Minimum isovalue:", &min_iso);
      spinner->set_int_limits( 0, 100 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT );
      
      spinner = new GLUI_Spinner( isolines, "Maximum isovalue:", &max_iso);
      spinner->set_int_limits( 0, 100 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT );      
      
      spinner = new GLUI_Spinner( isolines, "Number of Isolines :", &nr_isolines);
      spinner->set_int_limits( 0, 100 );
      spinner->set_alignment( GLUI_ALIGN_RIGHT ); 



      
      listbox = new GLUI_Listbox(isolines, "Isolines Colormap", &iso_col);
      listbox->add_item(0,"Black and White");
      listbox->add_item(1,"Rainbow");
      listbox->add_item(3,"Heatmap");
      listbox->add_item(4,"Green Fire"); 

      GLUI_Panel *heightmap = glui->add_panel ("HeightMap");
      checkbox = new GLUI_Checkbox( heightmap, "3d on", &draw_3d, 1, control_cb );
      
      
      
  glui->set_main_gfx_window( mainWindow );
 // GLUI_Master.set_glutIdleFunc( idle ); 

}

void light() {
    GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_position[] = { -200.0, 600.0, 1500.0 };
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glShadeModel (GL_SMOOTH);

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL,GL_SEPARATE_SPECULAR_COLOR);
}


void displayText()
{
  printf("Fluid Flow Simulation and Visualization\n");
	printf("=======================================\n");
	printf("Click and drag the mouse to steer the flow!\n");
	printf("T/t:   increase/decrease simulation timestep\n");
	printf("S/s:   increase/decrease hedgehog scaling\n");
	printf("c:     toggle direction coloring on/off\n");
	printf("V/v:   increase decrease fluid viscosity\n");
	printf("x:     toggle drawing matter on/off\n");
	printf("y:     toggle drawing hedgehogs on/off\n");
	printf("m:     toggle thru scalar coloring\n");
	printf("a:     toggle the animation on/off\n");
	printf("k:     select number of colors \n");
	printf("j:     min..max \n");
	printf("q:     quit\n\n");
}
//main: The main program
int main(int argc, char **argv)
{
	displayText();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(600,600);
	mainWindow = glutCreateWindow("Real-time smoke simulation and visualization");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMotionFunc(drag);

	init_simulation(DIM);	//initialize the simulation data structures
	
		
	
	call_GLUI();
	
	glutMainLoop();	

	//calls do_one_simulation_step, keyboard, display, drag, reshape
	
	
	return 0;
}
