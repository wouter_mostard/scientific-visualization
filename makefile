OBJECTS     = fluids.o colors.o glui_files.o visualize.o legend.o simulations.o isolines.o
CFILES      = $(OBJECTS:.o=.c)
EXECFILE    = smoke
INCLUDEDIRS = -I./fftw-2.1.5/include/ -I./glui-2.36/src/include/
LIBDIRS     = -L./fftw-2.1.5/lib/ -L./glui-2.36/src/lib/
LIBS        = -lglut -lrfftw -lfftw -lGL -lGLU -lm -lglui
#Possible flags for release (ffast-math uses less precision for floating-point numbers, check that your application can handle this)
#CFLAGS      = -O3 -march=x86-64 -mtune=generic -DNDEBUG -mfpmath=sse -ffast-math -Wall -pipe
#Debug flags
CFLAGS      = -ggdb -Wall -pipe 



.SILENT:

all: $(EXECFILE)

$(EXECFILE): $(OBJECTS)
		g++ $(LINKFLAGS) $(OBJECTS) -o $(EXECFILE) $(LIBDIRS) $(LIBS)

.c.o: $$@.c $$@.h
		g++ $(CFLAGS) $(INCLUDEDIRS) -c $<

clean:
		-rm -rf $(OBJECTS) $(EXECFILE)

depend:
		g++ -MM $(CFILES) > make.dep
example1:
		g++ $(LINKFLAGS) $(LIBDIRS) $(LIBS) $(INCLUDEDIRS) -o example2 example2.cpp 
-include make.dep