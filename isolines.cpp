#include "header.hh" 

extern fftw_real *rho, *rho0;

float calc_intersect(float pi, float pj, fftw_real vi, fftw_real vj, float v_init)
{
  float q;
  q = (pi*(vj-v_init) + pj*(v_init -vi))/(vj-vi);
  return q;
}


void  draw_isolines()
{
  float cur_min = min_iso/100.0;
  float cur_max = max_iso/100.0;
  float cur_rho;
  float hor_x, hor_y, ver_x, ver_y;
  int k, i,j, idx;
  fftw_real  wn = (fftw_real)winWidth / (fftw_real)(DIM + 1);   // Grid cell width
  fftw_real  hn = (fftw_real)winHeight / (fftw_real)(DIM + 1);  // Grid cell heigh
  int ul, ur, dr, dl;
  float R,G,B;
 
  for (k = 0; k <= nr_isolines; k++)
  {
    
    if (nr_isolines == 1)
    {
      cur_rho = cur_min;
    }
    else
    {
    cur_rho = cur_min+ k*(cur_max-cur_min)/(nr_isolines-1);
    }
  
    glBegin(GL_LINES);
    
    if (iso_col==COLOR_BLACKWHITE)
	  R = G = B = cur_rho;
    else if (iso_col==COLOR_RAINBOW)
	rainbow(cur_rho,&R,&G,&B);
    else if (iso_col == COLOR_HEAT)
	heatMap(cur_rho, &R, &G, &B);
    else if (iso_col == COLOR_GREEN)
	  greenMap(cur_rho, &R, &G, &B);      

    glColor3f(R,G,B);
    
    for(i = 0; i < DIM - 1 ; i++)
      for(j = 0; j < DIM - 1 ; j ++)
      {
	ul = ur = dr = dl = 0;
	
	idx = (j * DIM) + i;
	
	if (rho[idx] > cur_rho)
	  ul = 1;
	
	if (rho[idx+1] > cur_rho)
	  ur = 1;
	
	if (rho[idx+50+1] > cur_rho)
	  dr = 1;
	
	if (rho[idx+50] > cur_rho)
	  dl = 1;
	
	if ( ( ul == 1 ) && ( ur == 0 ) && ( dr == 0 ) && ( dl == 0 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx], rho[idx+1], cur_rho);
	  hor_y = j;
	  
	  ver_x = i;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx], rho[idx+50], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}

	if ( ( ul == 0 ) && ( ur == 1 ) && ( dr == 0 ) && ( dl == 0 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx], rho[idx+1], cur_rho);
	  hor_y = j;
	  
	  ver_x = i + 1;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx + 1 ], rho[idx+50 + 1], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}
	
	if ( ( ul == 0 ) && ( ur == 0 ) && ( dr == 1 ) && ( dl == 0 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx + 50 ], rho[idx+1 + 50], cur_rho);
	  hor_y = j + 1;
	  
	  ver_x = i + 1;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx + 1], rho[idx+50 + 1], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}      
	
	if ( ( ul == 0 ) && ( ur == 0 ) && ( dr == 0 ) && ( dl == 1 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx + 50], rho[idx+1 + 50], cur_rho);
	  hor_y = j+1;
	  
	  ver_x = i;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx], rho[idx+50], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}      
	
	if ( ( ul == 1 ) && ( ur == 1 ) && ( dr == 0 ) && ( dl == 0 ) ) 
	{
	  hor_x = i;
	  hor_y = calc_intersect(float(j), float(j+1), rho[idx], rho[idx+50], cur_rho);
	  
	  ver_x = i+1;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx + 1], rho[idx+50 + 1], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}   
	
	if ( ( ul == 0 ) && ( ur == 0 ) && ( dr == 1 ) && ( dl == 1 ) ) 
	{
	  hor_x = i;
	  hor_y = calc_intersect(float(j), float(j+1), rho[idx], rho[idx+50], cur_rho);
	  
	  ver_x = i+1;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx + 1], rho[idx+50 + 1], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}      

	if ( ( ul == 1 ) && ( ur == 0 ) && ( dr == 1 ) && ( dl == 0 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx], rho[idx+1], cur_rho);
	  hor_y = j;
	  
	  ver_x = i;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx], rho[idx+50], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	  
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx + 50 ], rho[idx+1 + 50], cur_rho);
	  hor_y = j + 1;
	  
	  ver_x = i + 1;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx + 1], rho[idx+50 + 1], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);	
	}      
	
	if ( ( ul == 0 ) && ( ur == 1 ) && ( dr == 0 ) && ( dl == 1 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx], rho[idx+1], cur_rho);
	  hor_y = j;
	  
	  ver_x = i + 1;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx + 1 ], rho[idx+50 + 1], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	  
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx + 50], rho[idx+1 + 50], cur_rho);
	  hor_y = j+1;
	  
	  ver_x = i;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx], rho[idx+50], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}      
	
	if ( ( ul == 1 ) && ( ur == 0 ) && ( dr == 0 ) && ( dl == 1 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx ], rho[idx+1], cur_rho);
	  hor_y = j;
	  
	  ver_x = calc_intersect(float(i), float(i+1), rho[idx + 50], rho[idx+1 + 50], cur_rho);
	  ver_y = j+1;
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}   
	
	if ( ( ul == 0 ) && ( ur == 1 ) && ( dr == 1 ) && ( dl == 0 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx ], rho[idx+1], cur_rho);
	  hor_y = j;
	  
	  ver_x = calc_intersect(float(i), float(i+1), rho[idx + 50], rho[idx+1 + 50], cur_rho);
	  ver_y = j+1;
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}         
	
      if ( ( ul == 0 ) && ( ur == 1 ) && ( dr == 1 ) && ( dl == 1 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx], rho[idx+1], cur_rho);
	  hor_y = j;
	  
	  ver_x = i;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx], rho[idx+50], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}

	if ( ( ul == 1 ) && ( ur == 0 ) && ( dr == 1 ) && ( dl == 1 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx], rho[idx+1], cur_rho);
	  hor_y = j;
	  
	  ver_x = i + 1;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx + 1 ], rho[idx+50 + 1], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}
	
	if ( ( ul == 1 ) && ( ur == 1 ) && ( dr == 0 ) && ( dl == 1 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx + 50 ], rho[idx+1 + 50], cur_rho);
	  hor_y = j + 1;
	  
	  ver_x = i + 1;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx + 1], rho[idx+50 + 1], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}      
	
	if ( ( ul == 1 ) && ( ur == 1 ) && ( dr == 1 ) && ( dl == 0 ) ) 
	{
	  hor_x = calc_intersect(float(i), float(i+1), rho[idx + 50], rho[idx+1 + 50], cur_rho);
	  hor_y = j+1;
	  
	  ver_x = i;
	  ver_y = calc_intersect(float(j),float( j + 1), rho[idx], rho[idx+50], cur_rho);
	  
	  glVertex2f(wn + (fftw_real)hor_x * wn, hn + (fftw_real)hor_y * hn);
	  glVertex2f(wn + (fftw_real)ver_x * wn, hn + (fftw_real)ver_y * hn);
	}            
	
      
  
    }
	  glEnd();
  }
}