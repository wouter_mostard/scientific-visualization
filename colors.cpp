#include "header.hh" 

extern fftw_real *vx, *vy;             //(vx,vy)   = velocity field at the current moment
extern fftw_real *vx0, *vy0;           //(vx0,vy0) = velocity field at the previous moment
extern fftw_real *fx, *fy;	            //(fx,fy)   = user-controlled simulation forces, steered with the mouse
extern fftw_real *rho, *rho0;			//smoke density at the current (rho) and previous (rho0) moment

extern int nr_col;

float max(float x, float y)
{ return x < y ? y : x; }



void rainbow(float value,float* R,float* G,float* B)
{
   const float dx=0.8;
   if (value<0) value=0;
   if (value>1) value=1;
   value = (6-2*dx)*value+dx;
   *R = max(0.0,(3-fabs(value-4)-fabs(value-5))/2);
   *G = max(0.0,(4-fabs(value-2)-fabs(value-4))/2);
   *B = max(0.0,(3-fabs(value-1)-fabs(value-2))/2);
}

void heatMap(float value, float* R, float* G, float* B){

    // If value <0 or >1 clamp the value to the boundry value (0 and 1)
    if (value < 0) value = 0;
    if (value > 1) value = 1;
      // This shouldn't fail because the values are bounded between 1 and 0.
      *R = 0;
      *G = 0;
      *B = 0;
      float thirds = 1.0 / 3.0;
      if (value <= thirds) {
  	  *R = value / thirds;    glPushMatrix();
      }
      else if (value <= thirds*2) {
	  *R = 1.0;
	  *G = value / (thirds*2);
      } else if (value <= 1) {
	  *R = 1.0;
	  *G = 1.0;
	  *B = value / 1.0;
      }
}

void greenMap(float value, float* R, float* G, float* B){
    // If value <0 or >1 clamp the value to the boundry value (0 and 1)
    if (value < 0) value = 0;
    if (value > 1) value = 1;

    // This shouldn't fail because the values are bounded between 1 and 0.
    *R = 0;
    *G = 0;
    *B = 0;
    float thirds = 1.0 / 3.0;
    if (value <= thirds) {
        *G = value / thirds;    glPushMatrix();
    }
    else if (value <= thirds*2) {
        *G = 1.0;
        *B = value / (thirds*2);
    } else if (value <= 1) {
        *B = 1.0;
        *G = 1.0;
        *R = value / 1.0;
    }
}

void zebraMap(float value, float* R, float* G,float* B){
    // If value <0 or >1 clamp the value to the boundry value (0 and 1)
  int i;  
  
  if (value < 0) value = 0;
    if (value > 1) value = 1;

    // This shouldn't fail because the values are bounded between 1 and 0.
    *R = 0;
    *G = 0;
    *B = 0;

    for (i=0;i<5; i++)
    {
    if (i*0.2 < value  && value < i*0.2 + 0.1 ) {
        *B = 1.0;
        *G = 1.0;
        *R = 1.0;
    }
    else if (i*0.2 + 0.1  < value && value < i*0.2 + 0.2 )
    {
        *B = 0.0;
        *G = 0.0;
        *R = 0.0;
    }
    else if (i*0.2 + 0.1 == value)
    {
        *B = 0.0;
        *G = 0.0;
        *R = 1.0;
    }
    }
}
void set_colormap(float vy, float min_val, float max_val )
{
   float R,G,B;
   if (nr_col > 1 && nr_col < 257)
     {
	vy = floor(vy *nr_col)/nr_col;

     }
     
     if (scale_col == 1)
     {
      vy = (vy-min_val)/(max_val-min_val); 
     }

     if(clamp_col){
	      float scaledMin = (float)minColor / 10;
	      float scaledMax = (float)maxColor / 10;
	      
	      if (vy < scaledMin)
		vy = scaledMin;
	      
	      if (vy > scaledMax)
		vy = scaledMax;
	      
	      vy = (vy - scaledMin) / (scaledMax-scaledMin);
     }
    
     
   if (scalar_col==COLOR_BLACKWHITE)
   {
      
	R = G = B = vy;

   }
   else if (scalar_col==COLOR_RAINBOW)
       rainbow(vy,&R,&G,&B);
   else if (scalar_col==COLOR_BANDS)
       {
          const int NLEVELS = 7;
          vy *= NLEVELS; vy = (int)(vy); vy/= NLEVELS;
	      rainbow(vy,&R,&G,&B);
	   }
    else if (scalar_col == COLOR_HEAT){
        heatMap(vy, &R, &G, &B);
    }
    else if (scalar_col == COLOR_GREEN){
        greenMap(vy, &R, &G, &B);      
    }
    else if (scalar_col == COLOR_ZEBRA){
	zebraMap(vy, &R, &G, &B);      
    } 

   glColor3f(R,G,B);
}

void select_color(int idx)
{
  if(glyph_select == 0){
    direction_to_color(fx[idx],fy[idx],0);
  }
  if(glyph_select == GLYPH_DENSITY){
    direction_to_color(rho[idx],0,GLYPH_DENSITY);
  }
  if(glyph_select == GLYPH_FF_MAGN){
    direction_to_color(fx[idx],fy[idx],GLYPH_FF_MAGN);
  }
  if(glyph_select == GLYPH_FV_MAGN){
    direction_to_color(vx[idx],vy[idx],GLYPH_FV_MAGN);
  }
}

void direction_to_color(float x, float y, int method)
{
	float r,g,b,f;
	if (method == 0)
	{
	  f = atan2(y,x) / 3.1415927 + 1;
	  r = f;
	  if(r > 1) r = 2 - r;
	  g = f + .66667;
	  if(g > 2) g -= 2;
	  if(g > 1) g = 2 - g;
	  b = f + 2 * .66667;
	  if(b > 2) b -= 2;
	  if(b > 1) b = 2 - b;
	}
	else if (method > 0 && method < 5)
	{
	  if ( y > 0 )
	  {
	  f= sqrt((x*x)+(y*y));
	  //float f_max = sqrt((maxvx*maxvx)+(maxvy*maxvy));
	  float f_max = 1.0;
	  f = f/f_max;
	  
	    heatMap(f,&r,&g,&b);
	  }else{
	    heatMap(x,&r,&g,&b);
	  }
	} 
	else
	{ r = g = b = 1; }
	glColor3f(r,g,b);
}
